import re

pass_length= re.compile(r'.{8,}')       
pass_check= re.compile(r'[A-Za-z0-9]')        
def pass_strength(text):
   
    if pass_length.search(text) == None:
        return False
    if pass_check.search(text) == None:
        return False
    else:
        return True

password = input('Enter password\n')
if pass_strength(password) is True:
    print(f'\'{password}\' is a Strong Password')
else:
    print(f'\'{password}\' is a Weak Password')