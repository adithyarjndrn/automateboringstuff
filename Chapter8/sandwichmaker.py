import pyinputplus as pyip
bread=pyip.inputMenu(['wheat', 'white', 'sourdough'])
protein=pyip.inputMenu(['chicken','turkey','ham','tofu'])
cheese=pyip.inputYesNo('Do you want Cheese?')
optional=pyip.inputYesNo('Do you want mayo, mustard, lettuce, or tomato?')
no=pyip.inputInt('Enter the quantity:',min=1)
cost=0
if(bread=='wheat' or bread=='white' or bread=='sourdough'):
    cost+=50
if(protein=='chicken' or protein=='ham'):
    cost+=100
if(protein=='turkey' or protein=='tofu'):
    cost+=120
if cheese=='yes':
    cost+=20
if optional=='yes':
    cost+=20
print('Net price:',cost*no)
