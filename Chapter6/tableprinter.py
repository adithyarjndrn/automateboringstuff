def print_table(table):
    column = [0] * len(table)
    count = 0
    while count < len(table):
        for item in table[count]:
            if len(item) > column[count]:
                column[count] = len(item)
        count += 1

    for word in range(len(table[0])):
        for item in range(len(table)):
            print(table[item][word].rjust(column[item]), end=' ')
        print()

tableData = [['apples', 'oranges', 'cherries', 'banana'],
              ['Alice', 'Bob', 'Carol', 'David'],
              ['dogs', 'cats', 'moose', 'goose']]

print_table(tableData)