#PracticeProblems

1. Functions reduce the length of the code. This makes programs shorter, easier to read, and easier to update.

2. Executes when function is called,not when function is defined

3. def statement creates a function

4. A function consists of the def statement and the code in it.A function call is what moves the program execution into the function

5. There is one global scope, and a local scope is created whenever a function is called.

6. When a function returns, the local scope is destroyed.
 
7. A return value is the value that a function call evaluates to

8. no return statement -return value=none

9. A global statement will force a variable in a function to refer to the global variable.

10. Nonetype

11. That import statement imports a module named areallyourpetsnamederic

12. spam.bacon().

13. use try and expect

14. Code tat cause error goes in try and if error happens goes in expect

