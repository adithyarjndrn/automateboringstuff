
1. empty list 

2. spam[2] = 'hello' 

3. 'd' 

4. 'd' (Negative indexes count from the end.)

5. ['a', 'b']

6. 1

7. [73.14, 'cat', 11, 'cat', True, 99]

8. [3.14, 11, 'cat', True]

9. The operator for list concatenation is +, while the operator for replication is *.

10. append() will add values only to the end of a list, insert() can add them anywhere in the list.

11. del statement and  remove() 

12. len(), have indexes and slices, be used in for loops, be concatenated

13. Lists are mutable and denoted by [ ]. Tuples are immutable and denoted by ( ) 

14. (42,)

15. tuple() and list() 

16. They contain references to list values.

