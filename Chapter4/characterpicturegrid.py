def rotate(g):
    return list(zip(*g[::-1]))

def print2DGrid(g):
    for row in range(len(g)):
        for col in range(len(g[row])):
            print(g[row][col], end='')

        print()

grid = [['.', '.', '.', '.', '.', '.'],
        ['.', 'O', 'O', '.', '.', '.'],
        ['O', 'O', 'O', 'O', '.', '.'],
        ['O', 'O', 'O', 'O', 'O', '.'],
        ['.', 'O', 'O', 'O', 'O', 'O'],
        ['O', 'O', 'O', 'O', 'O', '.'],
        ['O', 'O', 'O', 'O', '.', '.'],
        ['.', 'O', 'O', '.', '.', '.'],
        ['.', '.', '.', '.', '.', '.']]

gridRotated = rotate(grid)
print2DGrid(gridRotated)
