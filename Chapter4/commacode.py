from typing import List


def commacode(list):
    l=len(list)
    if l==0:
        return ''
    elif l==1:
        return list[0]
    else:
     for i in range(len(list)):
        return ','.join(list[:-1]) +' and '+ list[-1]
        
s=list(map(str,input().split()))
print(commacode(s))